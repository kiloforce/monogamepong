﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace PongGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Pong : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D backgroundImage;
        private Rectangle playerOne;
        private Rectangle playerTwo;
        private Rectangle ball;
        private int PLAYER_HEIGHT;
        private int PLAYER_WIDTH;
        private int PLAYER_SPEED;
        private int SCREEN_HEIGHT;
        private int SCREEN_WIDTH;
        private int BALL_SIZE;
        private int BALL_SPEED;
        private int ballYDirection;
        private int ballXDirection;
        private bool fullScreen = false;

        public Pong() : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Window.Title = "Dan's Pong Game";
            graphics.IsFullScreen = fullScreen;
            graphics.ApplyChanges(); //Don't know if this is really needed
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            PLAYER_HEIGHT = 100;
            PLAYER_WIDTH = 10;
            PLAYER_SPEED = 10;
            SCREEN_HEIGHT = graphics.PreferredBackBufferHeight;
            SCREEN_WIDTH = graphics.PreferredBackBufferWidth;
            BALL_SIZE = 20;
            BALL_SPEED = 5;
            ballXDirection = 1;
            ballYDirection = 1;
            playerOne = new Rectangle(20, 0, PLAYER_WIDTH, PLAYER_HEIGHT);
            playerTwo = new Rectangle(770, 0, PLAYER_WIDTH, PLAYER_HEIGHT);
            ball = new Rectangle(200, 200, BALL_SIZE, BALL_SIZE);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            backgroundImage = Content.Load<Texture2D>("background");

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState KBS = Keyboard.GetState();
            if (KBS.IsKeyDown(Keys.Escape))
                Exit();
            //Move player one
            if (KBS.IsKeyDown(Keys.W))
                playerOne.Y -= PLAYER_SPEED;
            else if (KBS.IsKeyDown(Keys.S))
                playerOne.Y += PLAYER_SPEED;
            //Move player two
            if (KBS.IsKeyDown(Keys.Up))
                playerTwo.Y -= PLAYER_SPEED;
            else if (KBS.IsKeyDown(Keys.Down))
                playerTwo.Y += PLAYER_SPEED;

            //Check player one out of bounds
            if (playerOne.Y < 0)
                playerOne.Y = 0;
            if ((playerOne.Y + PLAYER_HEIGHT) > SCREEN_HEIGHT)
                playerOne.Y = SCREEN_HEIGHT - PLAYER_HEIGHT;


            //Check player two out of bounds
            if (playerTwo.Y < 0)
                playerTwo.Y = 0;
            if ((playerTwo.Y + PLAYER_HEIGHT) > SCREEN_HEIGHT)
                playerTwo.Y = SCREEN_HEIGHT - PLAYER_HEIGHT;



            //Ball Collision with walls
            if (ball.Y < 0 || (ball.Y + BALL_SIZE) > SCREEN_HEIGHT)
                ballYDirection *= -1;
            if (ball.X < 0 || (ball.X + BALL_SIZE) > SCREEN_WIDTH)
                ballXDirection *= -1;

            //Ball Collision with Player One
            if (ball.X <= playerOne.X + PLAYER_WIDTH && ball.X >= playerOne.X && ball.Y >= playerOne.Y && ball.Y <= playerOne.Y + PLAYER_HEIGHT)
                ballXDirection = 1;

            //Ball Collision with Player Two
            if (ball.X + BALL_SIZE <= playerTwo.X + PLAYER_WIDTH && ball.X + BALL_SIZE >= playerTwo.X && ball.Y >= playerTwo.Y && ball.Y <= playerTwo.Y + PLAYER_HEIGHT)
                ballXDirection = -1;

            //Ball movement
            ball.Y += BALL_SPEED * ballYDirection;
            ball.X += BALL_SPEED * ballXDirection;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            // TODO: Add your drawing code here
            spriteBatch.Draw(backgroundImage, new Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT), Color.White);
            DrawRectangle(playerOne, Color.Red);
            DrawRectangle(playerTwo, Color.Green);
            DrawRectangle(ball, Color.Brown);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void DrawRectangle(Rectangle rect, Color color)
        {
            var blah = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
            blah.SetData(new[] { color });
            spriteBatch.Draw(blah, rect, color);
        }
    }
}